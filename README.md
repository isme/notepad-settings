# Notepad++ Settings #

Notepad++ is an awesome free text editor for Windows.

Its macro system lets you define repeatable edit sequences so you can quickly manipulate data in bulk.

This repo is a collection of macros I find useful.

### How do I get set up? ###

Install Notepad++.

Find the settings folder. The default location is `"%APPDATA%\Notepad++".

If the folder is not empty, delete everything in the folder. This will reset Notepad++ to its default settings, and will allow you to clone the custom settings into the repo.

Clone the repo into the settings folder.

Start Notepad++ and check that the new macros are available.

If they are not available, Notepad++ may have overwritten them with its default macros. If this happens, close Notepad++ and use Git to discard the changes to settings.xml. (TODO: Confirm that this happens.)

### What macros are there? ###

#### Convert TSV to Jira table ####

Run this macro to prepare tabular data for inclusion in a Jira message.

The document should contain tabular data in TSV format.

After running the macro, the document should contain the same tabular data formatted using Jira's table syntax.

The macro should work from any cursor position.

#### Convert TSV to ScrewTurn wiki table ####

Run this macro to prepare tabular data for inclusion in a ScrewTurn wiki page.

The document should contain tabular data with a header in TSV format.

After running the macro, the document should contain the same tabular data formatted using ScrewTurn Wiki's table syntax.

The macro should work from any cursor position.

What the macro does in plain English:

```
Replace newline with newline |- newline | space
Go to start
Type {| newline ! space
Select to the end of the line
Replace in selection tab with space !! space
Unselect
Replace tab with space || space
Go to end
Type newline |}
```

#### Convert newlines to commaspaces ####

Run this macro to convert a list of items delimited with newlines to a a list of items on a single line delimited with commaspaces.

The macro should work from any cursor position.

The snippet aggregates all the rows from sys.database\_files from each database into the temp table, and calls intrinsic functions to get the missing information for each file.

### Convert Jira table to SQL temp table ####

Run this macro to prepare a Jira data table for analysis in SQL Server.

The document should contain tabular data in Jira table markup format.

After running the macro, the document should contain the commands to populate a temporary table with the same data.

Jira table values have no type information, so all the SQL table columns are assumed to be strings of type NVARCHAR(MAX).

The macro should work from any cursor position.

This may not work correctly for values that contain single-quote characters or column names that contain square brackets.

The header is converted to a table definition like this:

* Replace "^\|\|" with "CREATE TABLE #DataFromJira (\r\n [".
* Replace " \|\| " with "] NVARCHAR\(MAX\),\r\n  [".
* Replace " \|\|$" with "] NVARCHAR\(MAX\)\r\n\);\r\n".

The rows are converted to insert statements like this:

* Replace "^\| " with "INSERT INTO #DataFromJira VALUES \(N'".
* Replace " \| " with "', N'".
* Replace " \|$" with "'\);".

#### Copy Mixcloud tracklist copypasta ####

Run this macro to convert a Mixcloud tracklist into "N. Artist - Title" format.

The document should contain a Mixcloud tracklist copied and pasted from the tracklist

Tested with Chrome and the [Mixcloud Tracklist browser extension](https://github.com/adlawson/mixcloud-tracklist).

### Notes ###

The Notepad++ documentation has a page called [Editing Configuration Files](http://docs.notepad-plus-plus.org/index.php/Editing_Configuration_Files) which documents the formats of all the files stored here.

Git can clone only into an empty folder, so we have to delete the existing settings.

This could be annoying if you already have custom settings you want to keep.

If you want to keep your existing settings (save for the macros), move everything to a temp directory before you clone, and then copy evberything back except settings.xml after the clone.

### Got a question about these scripts? ###

Create an issue in the [Issue Tracker](https://bitbucket.org/isme/notepad-settings/issues?status=new&status=open) and I'll be happy to help.